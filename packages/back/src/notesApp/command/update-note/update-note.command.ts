import { ICommand } from '@nestjs/cqrs';
import { NoteDto } from 'src/notesApp/entities/note-sample-helper/note.dto';
import { UpdateNoteDto } from 'src/notesApp/entities/note-sample-helper/update-note.dto';

export class UpdateNoteCommand implements ICommand {
  constructor(public updateNote: UpdateNoteDto) {}
}
