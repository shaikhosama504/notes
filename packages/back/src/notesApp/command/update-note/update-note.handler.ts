import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { NoteAggregateService } from 'src/notesApp/aggregates/note-aggregate/note-aggregate.service';
import { UpdateNoteCommand } from './update-note.command';

@CommandHandler(UpdateNoteCommand)
export class UpdateNoteCommandHandler
  implements ICommandHandler<UpdateNoteCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: NoteAggregateService,
  ) {}
  async execute(command: UpdateNoteCommand) {
    const { updateNote } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.updateNote(updateNote);
    aggregate.commit();
  }
}
