import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { NoteAggregateService } from 'src/notesApp/aggregates/note-aggregate/note-aggregate.service';
import { NoteEntity } from 'src/notesApp/entities/note-sample-helper/note.entity';
import { RemoveNoteCommand } from './remove-note.command';

@CommandHandler(RemoveNoteCommand)
export class RemoveNoteCommandHandler
  implements ICommandHandler<RemoveNoteCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: NoteAggregateService,
  ) {}
  async execute(command: RemoveNoteCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.removeNote(uuid);
    aggregate.commit();
  }
}
