import { ICommand } from '@nestjs/cqrs';

export class RemoveNoteCommand implements ICommand {
  constructor(public uuid: string) {}
}
