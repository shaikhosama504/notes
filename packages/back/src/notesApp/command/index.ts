import { AddNoteCommandHandler } from './add-note/add-note.handler';
import { RemoveNoteCommandHandler } from './remove-note/remove-note.handler';
import { UpdateNoteCommandHandler } from './update-note/update-note.handler';

export const NoteCommandManager = [
  AddNoteCommandHandler,
  UpdateNoteCommandHandler,
  RemoveNoteCommandHandler,
];
