import { ICommand } from '@nestjs/cqrs';
import { NoteDto } from 'src/notesApp/entities/note-sample-helper/note.dto';

export class AddNoteCommand implements ICommand {
  constructor(public note: NoteDto) {}
}
