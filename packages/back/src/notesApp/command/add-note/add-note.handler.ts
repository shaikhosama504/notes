import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { NoteAggregateService } from 'src/notesApp/aggregates/note-aggregate/note-aggregate.service';
import { AddNoteCommand } from './add-note.command';

@CommandHandler(AddNoteCommand)
export class AddNoteCommandHandler implements ICommandHandler<AddNoteCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: NoteAggregateService,
  ) {}
  async execute(command: AddNoteCommand) {
    const { note } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addNote(note);
    aggregate.commit();
  }
}
