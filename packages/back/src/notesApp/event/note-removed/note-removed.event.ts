import { IEvent } from '@nestjs/cqrs';
import { NoteEntity } from 'src/notesApp/entities/note-sample-helper/note.entity';

export class NoteRemovedEvent implements IEvent {
  constructor(public noteEntity: NoteEntity) {}
}
