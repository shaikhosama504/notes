import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { NoteSampleHelperService } from 'src/notesApp/entities/note-sample-helper/note-sample-helper.service';
import { NoteRemovedEvent } from './note-removed.event';

@EventsHandler(NoteRemovedEvent)
export class NoteRemovedHandler implements IEventHandler<NoteRemovedEvent> {
  constructor(private readonly noteService: NoteSampleHelperService) {}
  async handle(event: NoteRemovedEvent) {
    const { noteEntity: noteEntity } = event;
    await this.noteService.deleteOne({ uuid: noteEntity.uuid });
  }
}
