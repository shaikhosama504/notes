import { NoteAddedHandler } from './note-added/note-added-event.handler';
import { NoteRemovedHandler } from './note-removed/note-removed-event.handler';
import { NoteUpdatedHandler } from './note-updated/note-updated-event.handler';

export const NoteEventManager = [
  NoteAddedHandler,
  NoteUpdatedHandler,
  NoteRemovedHandler,
];
