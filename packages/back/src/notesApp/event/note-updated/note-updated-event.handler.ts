import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { NoteSampleHelperService } from 'src/notesApp/entities/note-sample-helper/note-sample-helper.service';
import { NoteUpdatedEvent } from './note-updated.event';

@EventsHandler(NoteUpdatedEvent)
export class NoteUpdatedHandler implements IEventHandler<NoteUpdatedEvent> {
  constructor(private readonly noteService: NoteSampleHelperService) {}

  async handle(event: NoteUpdatedEvent) {
    const { updateNote } = event;
    await this.noteService.updateOne(
      { uuid: updateNote.uuid },
      { $set: updateNote },
    );
  }
}
