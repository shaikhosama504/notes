import { IEvent } from '@nestjs/cqrs';
import { NoteEntity } from 'src/notesApp/entities/note-sample-helper/note.entity';

export class NoteUpdatedEvent implements IEvent {
  constructor(public updateNote: NoteEntity) {}
}
