import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { NoteSampleHelperService } from 'src/notesApp/entities/note-sample-helper/note-sample-helper.service';
import { NoteAddedEvent } from './note-added.event';

@EventsHandler(NoteAddedEvent)
export class NoteAddedHandler implements IEventHandler<NoteAddedEvent> {
  constructor(private readonly noteService: NoteSampleHelperService) {}
  async handle(event: NoteAddedEvent) {
    const { noteEntity } = event;
    await this.noteService.create(noteEntity);
  }
}
