import { Module } from '@nestjs/common';
import { NoteHelperController } from './controller/note-helper/note-helper.controller';
import { NoteCommandManager } from './command';
import { NoteEventManager } from './event';
import { NoteEntitiesModule } from './entities/entity.module';
import { NoteAggregateManager } from './aggregates';
import { NoteQueryManager } from './query';

@Module({
  imports: [NoteEntitiesModule],
  controllers: [NoteHelperController],
  providers: [
    ...NoteAggregateManager,
    ...NoteEventManager,
    ...NoteCommandManager,
    ...NoteQueryManager,
  ],
  exports: [...NoteAggregateManager, NoteEntitiesModule],
})
export class NotesModule {}
