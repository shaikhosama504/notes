import {
  BadRequestException,
  Controller,
  Get,
  Post,
  Body,
} from '@nestjs/common';
import { NoteDto } from '../../entities/note-sample-helper/note.dto';
import { UpdateNoteDto } from '../../entities/note-sample-helper/update-note.dto';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { AddNoteCommand } from 'src/notesApp/command/add-note/add-note.command';
import { GetNoteQuery } from 'src/notesApp/query/get-note/get-note.query';
import { UpdateNoteCommand } from 'src/notesApp/command/update-note/update-note.command';
import { RemoveNoteCommand } from 'src/notesApp/command/remove-note/remove-note.command';

@Controller('note-helper')
export class NoteHelperController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  createNote(@Body() note: NoteDto) {
    return this.commandBus.execute(new AddNoteCommand(note));
  }

  @Get('v1/read')
  async readNotes() {
    return await this.queryBus.execute(new GetNoteQuery());
  }

  @Post('v1/update')
  updateNote(@Body() note: UpdateNoteDto) {
    console.log(note);
    return this.commandBus.execute(new UpdateNoteCommand(note));
  }

  @Post('v1/delete')
  deleteNote(@Body() body) {
    if (!body.uuid) throw new BadRequestException('UUID must be presnt');
    return this.commandBus.execute(new RemoveNoteCommand(body.uuid));
  }
}
