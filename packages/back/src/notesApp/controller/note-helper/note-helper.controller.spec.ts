import { Test, TestingModule } from '@nestjs/testing';
import { NoteHelperController } from './note-helper.controller';
describe('NoteHelper Controller', () => {
  let controller: NoteHelperController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NoteHelperController],
    }).compile();

    controller = module.get<NoteHelperController>(NoteHelperController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
