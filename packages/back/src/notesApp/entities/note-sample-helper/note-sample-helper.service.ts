import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { NoteEntity } from './note.entity';
import { MongoRepository, Repository } from 'typeorm';
import { NoteDto } from './note.dto';

@Injectable()
export class NoteSampleHelperService {
  constructor(
    @InjectRepository(NoteEntity)
    private readonly noteRepository: MongoRepository<NoteEntity>,
  ) {}

  async create(note: NoteDto) {
    const noteObject = new NoteEntity();
    Object.assign(noteObject, note);
    return await noteObject.save();
  }

  async read() {
    return await this.noteRepository.find();
  }

  async updateOne(query, options?) {
    return await this.noteRepository.updateOne(query, options);
  }

  async findOne(param, options?) {
    return await this.noteRepository.findOne(param, options);
  }

  async deleteOne(param, options?) {
    return await this.noteRepository.deleteOne(param, options);
  }
}
