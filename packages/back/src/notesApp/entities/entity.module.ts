import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NoteSampleHelperService } from './note-sample-helper/note-sample-helper.service';
import { NoteEntity } from './note-sample-helper/note.entity';

@Module({
  imports: [TypeOrmModule.forFeature([NoteEntity])],
  providers: [NoteSampleHelperService],
  exports: [NoteSampleHelperService],
})
export class NoteEntitiesModule {}
