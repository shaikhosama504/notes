import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { NoteAggregateService } from 'src/notesApp/aggregates/note-aggregate/note-aggregate.service';
import { GetNoteQuery } from './get-note.query';

@QueryHandler(GetNoteQuery)
export class GetNoteQueryHandler implements IQueryHandler<GetNoteQuery> {
  constructor(private readonly manager: NoteAggregateService) {}

  async execute() {
    return this.manager.getNote();
  }
}
