import { GetNoteQueryHandler } from './get-note/get-note.handler';

export const NoteQueryManager = [GetNoteQueryHandler];
