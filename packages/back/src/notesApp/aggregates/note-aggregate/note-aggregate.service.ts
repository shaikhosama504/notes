import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { NoteSampleHelperService } from 'src/notesApp/entities/note-sample-helper/note-sample-helper.service';
import { NoteDto } from 'src/notesApp/entities/note-sample-helper/note.dto';
import { NoteEntity } from 'src/notesApp/entities/note-sample-helper/note.entity';
import { UpdateNoteDto } from 'src/notesApp/entities/note-sample-helper/update-note.dto';
import { NoteAddedEvent } from 'src/notesApp/event/note-added/note-added.event';
import { NoteRemovedEvent } from 'src/notesApp/event/note-removed/note-removed.event';
import { NoteUpdatedEvent } from 'src/notesApp/event/note-updated/note-updated.event';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class NoteAggregateService extends AggregateRoot {
  constructor(
    private readonly notesamplehelperService: NoteSampleHelperService,
  ) {
    super();
  }

  addNote(note: NoteDto) {
    const noteEntity = new NoteEntity();
    noteEntity.uuid = uuidv4();
    Object.assign(noteEntity, note);
    this.apply(new NoteAddedEvent(noteEntity));
  }

  async updateNote(note: UpdateNoteDto) {
    const provider = await this.notesamplehelperService.findOne({
      uuid: note.uuid,
    });
    const update = Object.assign(provider, note);
    this.apply(new NoteUpdatedEvent(update));
  }

  async getNote() {
    const provider = await this.notesamplehelperService.read();
    return provider;
  }

  async removeNote(uuid: string) {
    const found = await this.notesamplehelperService.findOne({ uuid });
    this.apply(new NoteRemovedEvent(found));
  }
}
