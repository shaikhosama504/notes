import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NotesModule } from './notesApp/notes.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TYPEORM_CONNECTION } from './constants/typeorm.connection';
import { ConfigModule } from './config/config.module';
import { CommonDepModule } from './common-dep/common-dep.module';

@Module({
  imports: [
    CommonDepModule,
    NotesModule,
    ConfigModule,
    TypeOrmModule.forRoot(TYPEORM_CONNECTION),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
