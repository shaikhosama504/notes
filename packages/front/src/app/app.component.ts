import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AppService } from './app.service';

import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Dialog } from './app.dialog';
import { Note } from './note';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  notes: any;
  public title = '';
  public description = '';
  public colors = [
    'white',
    '#FFE29F',
    '#DFFF9F',
    '#9FFFC9',
    '#9FE0FF',
    '#9FA3FF',
    '#E59FFF',
    '#FF9F9F',
  ];

  newNoteForm = new FormGroup({
    title: new FormControl(''),
    description: new FormControl(''),
  });

  toggle = false;
  edited = false;
  constructor(
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    private _appService: AppService
  ) {}

  color1 = 'white';

  toggleColor(color: any) {
    this.color1 = color;
    return color;
  }
  toggleColorCard(newColor: any, cardNote: any) {
    (cardNote.color = newColor),
      this._appService.patchNote(cardNote).subscribe(() => {});
  }

  ngOnInit() {
    this.fetchNote();
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  expandOn() {
    this.edited = true;
  }

  expandOff() {
    this.edited = false;
    this.newNoteForm.controls.title.setValue('');
    this.newNoteForm.controls.description.setValue('');
    const elm = document.querySelector<HTMLElement>('.input')!;
    elm.style.background = 'white';
  }

  doneNote() {
    if (
      this.newNoteForm.controls.title.value.length == 0 &&
      this.newNoteForm.controls.description.value.length == 0
    ) {
      this.openSnackBar('Note cannot be empty!', 'Ok');
    } else {
      const note: Note = {
        title: this.newNoteForm.controls.title.value,
        description: this.newNoteForm.controls.description.value,
        color: this.color1,
      };

      this._appService.addNote(note).subscribe((data) => this.fetchNote());
      this.newNoteForm.controls.title.setValue('');
      this.newNoteForm.controls.description.setValue('');
      note.color = 'white';
    }
  }

  fetchNote() {
    this._appService.getNote().subscribe((data) => (this.notes = data));
  }

  clearNote(data: string) {
    this._appService.deleteNote(data).subscribe(() => {});
    this.fetchNote();
  }

  openDialog(data: Note) {
    const dialogRef = this.dialog.open(Dialog, {
      data: {
        uuid: data.uuid,
        title: data.title,
        description: data.description,
        color: data.color,
      },
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe(() => this.fetchNote());
  }
}
