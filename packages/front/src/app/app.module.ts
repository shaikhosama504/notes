import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';
import { AppService } from './app.service';
import { MatGridListModule } from '@angular/material/grid-list';
import { Dialog } from './app.dialog';
import { MatInputAutosizeModule } from 'mat-input-autosize';
import { MatListModule } from '@angular/material/list';

@NgModule({
  declarations: [AppComponent, Dialog],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    MatSnackBarModule,
    MatIconModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatGridListModule,
    MatInputAutosizeModule,
    MatListModule,
  ],

  providers: [AppService],
  bootstrap: [AppComponent],
})
export class AppModule {}
