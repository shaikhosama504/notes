import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Note } from './note';

@Injectable()
export class AppService {
  private _url: string = 'http://localhost:3000/note-helper/';

  constructor(private http: HttpClient) {}

  addNote(data: any) {
    let _uurl: string = 'http://localhost:3000/note-helper/v1/create';
    return this.http.post<any>(_uurl, {
      title: data.title,
      description: data.description,
      color: data.color,
    });
  }

  getNote() {
    let _uurl: string = 'http://localhost:3000/note-helper/v1/read';
    return this.http.get<Note[]>(_uurl);
  }

  patchNote(data: any) {
    return this.http.post<any>('http://localhost:3000/note-helper/v1/update', {
      uuid: data.uuid,
      title: data.title,
      description: data.description,
      color: data.color,
    });
  }
  deleteNote(data: any) {
    return this.http.post<any>('http://localhost:3000/note-helper/v1/delete', {
      uuid: data.uuid,
      title: data.title,
      description: data.description,
      color: data.color,
    });
  }
}
