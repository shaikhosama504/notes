import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogRef,
} from '@angular/material/dialog';

import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { Note } from './note';

@Component({
  selector: 'app.dialog',
  templateUrl: 'app.dialog.html',
  styleUrls: ['./app.component.css'],
})
export class Dialog implements OnInit {
  public colors = [
    'white',
    '#FFE29F',
    '#DFFF9F',
    '#9FFFC9',
    '#9FE0FF',
    '#9FA3FF',
    '#E59FFF',
    '#FF9F9F',
  ];
  updateNoteForm = new FormGroup({
    title: new FormControl(''),
    description: new FormControl(''),
  });

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: Note,
    public appService: AppService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<AppComponent>,
    private _appService: AppService
  ) {}
  ngOnInit() {
    this.updateNoteForm.controls.title.setValue(this.data.title);
    this.updateNoteForm.controls.description.setValue(this.data.description);
  }

  toggleColorCard(newColor: any, cardNote: any) {
    (cardNote.color = newColor),
      this._appService.patchNote(cardNote).subscribe(() => {});
  }
  saveNote(data: Note) {
    if (
      this.updateNoteForm.controls.title.value.length == 0 &&
      this.updateNoteForm.controls.description.value.length == 0
    ) {
      this.appService.deleteNote(data).subscribe(() => {});
      this.dialogRef.close();
    } else {
      data.title = this.updateNoteForm.controls.title.value;
      data.description = this.updateNoteForm.controls.description.value;
      const elm = document.querySelector<HTMLElement>('.updateNoteCard')!;
      elm.style.background = data.color;
      this.appService.patchNote(data).subscribe(() => {});
      this.dialogRef.close();
    }
  }
}
