export interface Note{
    uuid?: string;
    title: string;
    description: string;
    color: string;
}